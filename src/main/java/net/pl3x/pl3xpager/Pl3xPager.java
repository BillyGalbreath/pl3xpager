package net.pl3x.pl3xpager;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

import net.pl3x.pl3xpager.commands.CmdPage;
import net.pl3x.pl3xpager.commands.CmdPl3xPager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.Song;

public class Pl3xPager extends JavaPlugin {
	public HashMap<String,Long> lastUsed = new HashMap<String,Long>();
	private boolean nbs = false;
	
	public void onEnable() {
		if (!new File(getDataFolder() + File.separator + "config.yml").exists())
			saveDefaultConfig();
		
		if (getServer().getPluginManager().isPluginEnabled("NoteBlockAPI")) {
			log("Detected NoteBlockAPI, enabling NBS support!");
			nbs = true;
		}
		
		this.getCommand("pl3xpager").setExecutor(new CmdPl3xPager(this));
		this.getCommand("page").setExecutor(new CmdPage(this));
		
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}
		
		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log(getName() + " Disabled.");
	}
	
	public void log (Object obj) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" +  getName() + "&3]&r " + obj));
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
	
	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
	
	public boolean playNBS(Player player, String jingle) {
		if (!nbs)
			return false;
		Song song = NBSDecoder.parse(new File(getDataFolder() + File.separator + "nbs" + File.separator + jingle + ".nbs"));
		song.addPlayer(player);
		song.setPlaying(true);
		return true;
	}
	
	public boolean hasNBS() {
		return nbs;
	}
}
