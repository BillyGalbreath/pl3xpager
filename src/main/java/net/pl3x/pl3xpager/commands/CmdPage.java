package net.pl3x.pl3xpager.commands;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Date;

import net.pl3x.pl3xpager.Pl3xPager;
import net.pl3x.pl3xpager.configuration.PlayerConfigManager;

import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdPage implements CommandExecutor {
	private Pl3xPager plugin;
	
	public CmdPage(Pl3xPager plugin) {
		this.plugin = plugin;
	}
	
	private Sound getSound(String s, Player p) {
		Sound sound = null;
		try {
			sound = Sound.valueOf(s.toUpperCase());
		} catch(Exception e) {
			return null;
		}
		return sound;
	}
	
	private boolean jingleExists(String jingle) {
		if (new File(plugin.getDataFolder() + File.separator + "nbs" + File.separator + jingle + ".nbs").exists())
			return true;
		return false;
	}
	
	private File[] getJingles() {
		File dir = new File(plugin.getDataFolder() + File.separator + "nbs");
		File[] list = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".nbs");
			}
		});
		Arrays.sort(list);
		return list;
	}
	
	private Float getPitch(String s, Player p) {
		Float pitch = 1F;
		try {
			pitch = Float.valueOf(s);
		} catch(Exception e) {
			p.sendMessage(plugin.colorize("&4Pitch MUST be a number!"));
			return null;
		}
		return pitch;
	}
	
	private Integer cooldownTimeLeft(Player p) {
		if (!plugin.lastUsed.containsKey(p.getName()))
			return 0;
		long now = new Date().getTime();
		long then = plugin.lastUsed.get(p.getName());
		Integer cooldown = plugin.getConfig().getInt("page-cooldown", 30) * 1000;
		if (then + cooldown > now)
			return Math.round((then + cooldown - now) / 1000);
		return 0;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("page"))
			return false;
		if (!cs.hasPermission("pl3xpager.page")) {
			cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
			return true;
		}
		if (args.length < 1) {
			cs.sendMessage(plugin.colorize("&4You MUST specify a player to page!"));
			return true;
		}
		Player p = (Player)cs;
		PlayerConfigManager pcm = PlayerConfigManager.getManager(p.getName());
		Sound bukkitSound = null;
		Float pitch = 1F;
		if (args[0].trim().equalsIgnoreCase("-enable")) {
			pcm.set("pager.enabled", true);
			pcm.forceSave();
			cs.sendMessage(plugin.colorize("&dPager enabled."));
			return true;
		} else if (args[0].trim().equalsIgnoreCase("-disable")) {
			pcm.set("pager.enabled", false);
			pcm.forceSave();
			cs.sendMessage(plugin.colorize("&dPager disabled."));
			return true;
		} else if (args[0].trim().equalsIgnoreCase("-list")) {
			if (plugin.hasNBS()) {
				cs.sendMessage(plugin.colorize("&dHere is a list of jingles you can use:"));
				File[] files = getJingles();
				StringBuilder sb = new StringBuilder();
				for (File file : files) {
					sb.append("&7" + file.getName().split(".nbs")[0]);
					sb.append("&d, ");
				}
				cs.sendMessage(plugin.colorize(sb.substring(0, sb.length() - 4)));
			}
			cs.sendMessage(plugin.colorize("&dFor a list of sounds, click this link:"));
			cs.sendMessage(plugin.colorize("&dhttp://jd.bukkit.org/rb/apidocs/org/bukkit/Sound.html"));
			return true;
		} else if (args[0].trim().equalsIgnoreCase("-set")) {
			if (args.length < 2) {
				cs.sendMessage(plugin.colorize("&4You MUST specify a SOUND to set!"));
				return true;
			}
			bukkitSound = getSound(args[1].trim(), p);
			if (bukkitSound == null) {
				if (plugin.hasNBS()) {
					if (!jingleExists(args[1].trim())) {
						p.sendMessage(plugin.colorize("&4The sound &7" + args[1].trim() + "&4 is not a valid Bukkit Sound or a supported Jingle!"));
						return true;
					}
					pcm.set("pager.sound", args[1].trim());
					pcm.set("pager.pitch", pitch);
					pcm.forceSave();
					cs.sendMessage(plugin.colorize("&dPager jingle set."));
					return true;
				} else {
					p.sendMessage(plugin.colorize("&4The sound &7" + args[1].trim() + "&4 is not a valid Bukkit Sound!"));
					return true;
				}
			}
			if (args.length > 2) {
				pitch = getPitch(args[2].trim(), p);
				if (pitch == null)
					return true;
			}
			pcm.set("pager.sound", bukkitSound.toString());
			pcm.set("pager.pitch", pitch);
			pcm.forceSave();
			cs.sendMessage(plugin.colorize("&dPager sound set."));
			return true;
		} else if (args[0].trim().equalsIgnoreCase("-test")) {
			if (args.length > 1) {
				bukkitSound = getSound(args[1].trim(), p);
				if (bukkitSound == null) {
					if (plugin.hasNBS()) {
						if (!jingleExists(args[1].trim())) {
							p.sendMessage(plugin.colorize("&4The sound &7" + args[1].trim() + "&4 is not a valid Bukkit Sound or a supported Jingle!"));
							return true;
						}
						plugin.playNBS(p, args[1].trim());
						cs.sendMessage(plugin.colorize("&dPlaying jingle."));
						return true;
					} else {
						p.sendMessage(plugin.colorize("&4The sound &7" + args[1].trim() + "&4 is not a valid Bukkit Sound!"));
						return true;
					}
				}
			}
			if (args.length > 2) {
				pitch = getPitch(args[2].trim(), p);
				if (pitch == null)
					return true;
			}
			if (bukkitSound == null) {
				String standard = plugin.getConfig().getString("default-sound", "ANVIL_LAND");
				String sound = pcm.getString("pager.sound", standard);
				bukkitSound = getSound(sound, p);
				if (bukkitSound == null)
					return true;
				pitch = (float) pcm.getDouble("pager.pitch", pitch);
			}
			p.playSound(p.getLocation(), bukkitSound, 10F, pitch);
			cs.sendMessage(plugin.colorize("&dPlaying sound."));
			return true;
		}
		Integer cooldownTimeLeft = cooldownTimeLeft(p);
		if (cooldownTimeLeft > 0) {
			cs.sendMessage(plugin.colorize("&4You must wait &7" + cooldownTimeLeft + "&4 more seconds to use this command!"));
			return true;
		}
		OfflinePlayer target = plugin.getServer().getOfflinePlayer(args[0].trim());
		if (target == null || !target.isOnline()) {
			cs.sendMessage(plugin.colorize("&4That player is not online!"));
			return true;
		}
		Player tp = (Player) target;
		pcm = PlayerConfigManager.getManager(tp.getName());
		if (!pcm.getBoolean("pager.enabled", true)) {
			p.sendMessage(plugin.colorize("&4That player has pager disabled!"));
			return true;
		}
		String standard = plugin.getConfig().getString("default-sound", "ANVIL_LAND");
		String sound = pcm.getString("pager.sound", null);
		bukkitSound = getSound(sound, p);
		boolean useJingle = false;
		if (bukkitSound == null) {
			if (plugin.hasNBS() && jingleExists(sound)) {
				useJingle = true;
			} else {
				try {
					bukkitSound = Sound.valueOf(standard);
				} catch(Exception e) {
					bukkitSound = Sound.ANVIL_LAND;
				}
			}
		}
		if (useJingle) {
			plugin.playNBS(tp, sound);
		} else {
			pitch = (float) pcm.getDouble("pager.pitch", pitch);
			tp.playSound(tp.getLocation(), bukkitSound, 10F, pitch);
		}
		plugin.lastUsed.put(p.getName(), new Date().getTime());
		p.sendMessage(plugin.colorize("&dPaging &7" + tp.getName() + "&d..."));
		tp.sendMessage(plugin.colorize("&d#####################################################"));
		tp.sendMessage(plugin.colorize("&d"));
		tp.sendMessage(plugin.colorize("&d You are being paged by &7" + p.getDisplayName() + "&d!"));
		tp.sendMessage(plugin.colorize("&d"));
		tp.sendMessage(plugin.colorize("&d#####################################################"));
		return true;
	}
}
