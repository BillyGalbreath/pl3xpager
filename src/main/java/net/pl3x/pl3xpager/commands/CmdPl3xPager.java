package net.pl3x.pl3xpager.commands;

import net.pl3x.pl3xpager.Pl3xPager;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdPl3xPager implements CommandExecutor {
	private Pl3xPager plugin;
	
	public CmdPl3xPager(Pl3xPager plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("pl3xpager"))
			return false;
		if (!cs.hasPermission("pl3xpager.pl3xpager")) {
			cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reloadConfig();
				cs.sendMessage(plugin.colorize("&d" + plugin.getName() + " configuration reloaded."));
				return true;
			}
		}
		cs.sendMessage(plugin.colorize("&d" + plugin.getName() + "&7 v" + plugin.getDescription().getVersion() + "&d."));
		return true;
	}
}
